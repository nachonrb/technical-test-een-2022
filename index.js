const express = require('express');
const app = express();
const axios = require('axios');
const dotenv = require('dotenv').config();

app.get('/ok', (req, res) => {
    res.status(200).send({
        key: 200,
        value: 'OK'
    })
});

app.post('/login/', (req, res) => {
    let username = process.env.USERNAME;
    let pass = process.env.PASS;
    let url = 'http://rest.cameramanager.com/oauth/token?grant_type=password&scope=write&username=' + username + '&password=' + pass;
    let auth = process.env.API_KEY + ':' + process.env.API_SECRET;
    let encodedAuth = 'Basic ' + btoa(auth);
    
    let config = {
        headers: {
            Authorization: encodedAuth,
        }
    }

    axios.post(url, '', config)
    .then(resp => {
        let values = {
            token: resp.data.access_token
        }
        res.status(200).send(values);
    }).catch(error => {
        console.error(error);
        res.status(401).send('Error');
    });

    res.status(200);
})


app.get('/cameras/', (req, res) => {

    let url = 'http://rest.cameramanager.com/rest/v2.4/cameras';
    let config = {
        headers: {
            Authorization: req.headers.authorization,
        }
    }

    axios.get(url, config)
    .then(resp => {
        res.status(200).send(resp.data);
    }).catch(error => {
        console.error(error);
        res.status(401).send('Error');
    });

})

app.get('/cameras/:id/', (req, res) => {

    let {id} = req.params;

    let url = 'http://rest.cameramanager.com/rest/v2.4/cameras/' + id + '/';
    let config = {
        headers: {
            Authorization: req.headers.authorization,
        }
    }

    axios.get(url, config)
    .then(resp => {
        res.status(200).send(resp.data);
    }).catch(error => {
        console.error(error);
        res.status(401).send('Error');
    });

})

app.listen(
    process.env.PORT,
    () => console.log('We are live')
);